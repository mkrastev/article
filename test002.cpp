#include <stdio.h>
#include "timer.h"

typedef const float (& immutable_array_of_16_floats)[16];

struct InputArray {
	float array[16];
	InputArray() {
		array[0] = 1;
		array[1] = 2;
		array[2] = 3;
		array[3] = 4;
		array[4] = 5;
		array[5] = 6;
		array[6] = 7;
		array[7] = 8;
		array[8] = 9;
		array[9] = 10;
		array[10] = 11;
		array[11] = 12;
		array[12] = 13;
		array[13] = 14;
		array[14] = 15;
		array[15] = 16;
	}

	operator immutable_array_of_16_floats () const {
		return array;
	}
};

const InputArray input;
float output[16];

#if POISON
#include <stdlib.h>
void* x = malloc(42);

#endif
int main(int, char**) {
	const uint64_t t0 = timer_ns();

	for (size_t i = 0; i < COUNT_OF((immutable_array_of_16_floats) input); ++i)
		output[i] = input[i] + input[i];

	const uint64_t dt = timer_ns() - t0;

	for (size_t i = 0; i < COUNT_OF(output); ++i)
		printf("%f", output[i]);

	printf("\nelapsed time: %f s\n", dt * 1e-9);
	return 0;
}
