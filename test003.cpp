#include <stdio.h>
#include "timer.h"

float inputA[] __attribute__ ((aligned(64))) = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
float inputB[] __attribute__ ((aligned(64))) = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
float res[16] __attribute__ ((aligned(64)));

void add(
    const float (& a)[16],
    const float (& b)[16],
	float (& res)[16]) {

	for (size_t i = 0; i < 16; ++i)
		res[i] = a[i] * b[i];
}

int main(int, char**) {
	const uint64_t t0 = timer_ns();

	add(inputA, inputB, res);

	const uint64_t dt = timer_ns() - t0;

	for (size_t i = 0; i < COUNT_OF(res); ++i)
		printf("%f", res[i]);

	printf("\nelapsed time: %f s\n", dt * 1e-9);
	return 0;
}
