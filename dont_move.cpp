// Can we benefit from && references without the hassle known as 'move semantics'?
//
// Let's start by a simple, practical categorization of values:
// ■ mutable variable
// ■ immutable variable
// ■ temporary
//
// note: in C++ every object, whether named or temporary, can be queried about its address; don't be confused by lvalue-vs-rvalue legacy terminology
//
// Traditionally, temporaries -- unnamed results from expressions -- serve the purpose of carrying values for function arguments, or getting assigned to variables, whether by-value or by-const-ref.
// But a temporary can be of further use to expressions -- imagine if a temporary can be both input to an expression AND the result of the expression, representing the datum, or 'the state' over which
// the expression operates. Consider the way named objects can be used as the datum for some composite computation:
//
// T x;
// x.mutatorA();
// x.mutatorB();
// x.mutatorC();
//
// The advantage of a temporary being used that way, is that the temporary itself could be the result from a prior computation, thus chaining expressions of arbitrary length without
// the need for either move or copy semantics. Everything would be by-ref, save for an initial emitter of temporaries.
//
// Let foo() be an emitter (or a factory) of temporaries of type T. Additionally, let bar() and bas() be functions that take an object of type T by-ref, mutate it (if possible), and return it, just like
// the class mutators above. As the result of any such chain of computations, we'd get either a mutable output -- so we could further mutate it, or an immutable output once we are done mutating it.
//
// Prior to C++11, only immutable results would be possible:
//
// const T& x = foo(); // ok
// const T& y = bas(bar(foo())); // might be ok: foo produces a temporary, thus bar could consume that only as a const ref;
//                               // single-datum composition possible only if bar and bas produced their own temporaries and copy elision worked every time
//
// Since C++11 that can be solved without bothering with copy elision or move semantics, as both formal args to functions AND declarations can be refs to mutable temporaries:
//
// T&& x = foo(); // ok; x is mutable so we can keep mutating it
// T&& y = bas(bar(foo())); // ok for the compiler if bar and bas took mutable refs -- that would be single-datum composition if it worked as written, alas (see notes further down at composition tests)
// const T& z = bas(bar(foo()); // same as the above, only without possibility for further mutations

#include <stdio.h>

// Skeleton type whose purpose is to track copy ops (ctor/assignment) while exposing mock-up functionality
struct A {
	A() {}

#if TRACE_DTOR
	~A() {
		fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	}

#endif
	A(const A& arg) {
		fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	}

	A& operator =(const A& arg) {
		fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
		return *this;
	}

	A& mutate() {
		fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
		return *this;
	}

	const A& access() const {
		fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
		return *this;
	}

	A operator +(const A& rhs) const {
		return A();
	}
};

// Some return-by-value producer of the above skeleton type -- deals exclusively with temporaries
A foo() {
	A a;
	A b;
	return a + b;
}

//////////////////////////////////////////////////////////////////////
// bar overloads -- full differentiation of argument categories
//////////////////////////////////////////////////////////////////////

template < typename T >
T&& bar(T&& arg) { // arg is a temporary (thus mutable by this routine)
	fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	arg.mutate();
	return static_cast< T&& >(arg);
}

template < typename T >
T& bar(T& arg) { // arg is a mutable variable
	fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	return arg.mutate();
}

template < typename T >
const T& bar(const T& arg) { // arg is an immutable variable
	fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	return arg.access();
}

//////////////////////////////////////////////////////////////////////
// bas overloads -- minimal differentiation of argument categories
//////////////////////////////////////////////////////////////////////

template < typename T >
T&& bas(T&& arg) { // arg is a temporary OR a mutable variable
	fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	arg.mutate();
	return static_cast< T&& >(arg);
}

template < typename T >
const T& bas(const T& arg) { // arg is an immutable variable
	fprintf(stderr, "%s, %d\n", __FUNCTION__, __LINE__);
	return arg.access();
}


int main(int, char**) {
	fprintf(stderr, "■ initialization\n");

	A a = foo(); // return-value optimisation (RVO) -- no copy ctor
	const A& b = foo(); // reference of a temporary -- no copy ctor
	const A c = a; // copy ctor goes here

	fprintf(stderr, "■ overload resolution\n");

	bar(a);
	bar(b);
	bar(c);
	bar(foo());

	fprintf(stderr, "■ composition -- mutable datum, mutable result\n");

	// Ideally, we'd write:
	//
	// A&& d = bas(bar(foo()))
	//
	// but we'd lose the temp after the evaluation of the RHS, even if the compiler knew we were reusing the same temp all the way to d!

	A&& d = foo(); // capture the temp so it does not vanish
	bas(bar(d));

	fprintf(stderr, "■ composition -- mutable datum, immutable result\n");

	A&& mutable_e = foo(); // capture the temp so it does not vanish
	bas(bar(mutable_e));
	const A& e = mutable_e; // refrain from using both aliases in an expression; such aliasing could be ruled out if the language allowed for ref stealing:
	                        // const T& new_ref = old_ref delete; // new_ref supersedes old_ref; old_ref becomes deleted through the end of the scope; nothing changes from the POV of the referred object

	fprintf(stderr, "■ preservation of constness\n");

	bas(a);
	bas(b);
	bas(c);

	return 0;
}
