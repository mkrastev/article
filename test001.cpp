#include <stdio.h>
#include "timer.h"

const float input[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
float output[COUNT_OF(input)];

#if POISON
#include <stdlib.h>
void* x = malloc(42);

#endif
int main(int, char**) {
	const uint64_t t0 = timer_ns();

	for (size_t i = 0; i < COUNT_OF(input); ++i)
		output[i] = input[i] + input[i];

	const uint64_t dt = timer_ns() - t0;

	for (size_t i = 0; i < COUNT_OF(output); ++i)
		printf("%f", output[i]);

	printf("\nelapsed time: %f s\n", dt * 1e-9);
	return 0;
}
