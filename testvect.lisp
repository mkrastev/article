;;;; matrix musings
(declaim (optimize (speed 3) (compilation-speed 0) (debug 0) (safety 0)))
(declaim (inline vec4-empty))
(declaim (inline vec4-make))
(declaim (inline vec4-coerce))
(declaim (inline vec4-coerce-vec))
(declaim (inline vec4-dump))
(declaim (inline vec4-mul-scal))
(declaim (inline vec4-in-mat4-mul-scal))
(declaim (inline vec4-in-mat4-madd-scal))
(declaim (inline mat4-empty))
(declaim (inline mat4-coerce))
(declaim (inline mat4-dump))

;;; via simple-array
(deftype vec4 () '(simple-array single-float (4)))
(deftype mat4 () '(simple-array single-float (16)))

(defun vec4-empty()
	(make-array 4 :element-type 'single-float :adjustable nil :fill-pointer nil :displaced-to nil :initial-element 0.0)
)

(defun vec4-make(x y z w)
	(declare (type single-float x))
	(declare (type single-float y))
	(declare (type single-float z))
	(declare (type single-float w))
	(make-array 4 :element-type 'single-float :adjustable nil :fill-pointer nil :displaced-to nil :initial-contents (list x y z w))
)

(defun vec4-coerce(x y z w)
	(vec4-make
		(coerce x 'single-float)
		(coerce y 'single-float)
		(coerce z 'single-float)
		(coerce w 'single-float)
	)
)

(defun vec4-coerce-vec(vec)
	;; (check-type vec (simple-vector 4))
	(declare (type (simple-vector 4) vec))
	(vec4-coerce
		(aref vec 0)
		(aref vec 1)
		(aref vec 2)
		(aref vec 3)
	)
)

(defun vec4-dump(vec)
	;; (check-type vec vec4)
	(declare (type vec4 vec))
	(format t "~8,4F  ~8,4F  ~8,4F  ~8,4F~%"
		(aref vec 0)
		(aref vec 1)
		(aref vec 2)
		(aref vec 3)
	)
)

(defun vec4-mul-scal(out vec scal)
	(declare (type vec4 out))
	(declare (type vec4 vec))
	(declare (type single-float scal))
	(let ((out-dimension (array-dimension out 0)))
		(dotimes (i out-dimension out)
			(setf (aref out i) (* (aref vec i) scal))
		)
	)
)

(defun vec4-in-mat4-mul-scal(out-base out vec scal)
	(declare (type fixnum out-base))
	(declare (type mat4 out))
	(declare (type mat4 vec))
	(declare (type single-float scal))
	(let ((row-dimension 4))
		(dotimes (i row-dimension)
			;; out[out-base + i] := vec[i] * scal
			(setf (aref out (+ out-base i)) (* (aref vec i) scal))
		)
	)
)

(defun vec4-in-mat4-madd-scal(out-base out vec-base vec scal)
	(declare (type fixnum out-base))
	(declare (type mat4 out))
	(declare (type fixnum vec-base))
	(declare (type mat4 vec))
	(declare (type single-float scal))
	(let ((row-dimension 4))
		(dotimes (i row-dimension)
			;; out[out-base + i] :+= vec[vec-base + i] * scal
			(incf (aref out (+ out-base i)) (* (aref vec (+ vec-base i)) scal))
		)
	)
)

(defun mat4-mul(out lhs rhs)
	(declare (type mat4 out))
	(declare (type mat4 lhs))
	(declare (type mat4 rhs))

	(vec4-in-mat4-mul-scal   0 out    rhs (aref lhs  0))
	(vec4-in-mat4-madd-scal  0 out  4 rhs (aref lhs  1))
	(vec4-in-mat4-madd-scal  0 out  8 rhs (aref lhs  2))
	(vec4-in-mat4-madd-scal  0 out 12 rhs (aref lhs  3))

	(vec4-in-mat4-mul-scal   4 out    rhs (aref lhs  4))
	(vec4-in-mat4-madd-scal  4 out  4 rhs (aref lhs  5))
	(vec4-in-mat4-madd-scal  4 out  8 rhs (aref lhs  6))
	(vec4-in-mat4-madd-scal  4 out 12 rhs (aref lhs  7))

	(vec4-in-mat4-mul-scal   8 out    rhs (aref lhs  8))
	(vec4-in-mat4-madd-scal  8 out  4 rhs (aref lhs  9))
	(vec4-in-mat4-madd-scal  8 out  8 rhs (aref lhs 10))
	(vec4-in-mat4-madd-scal  8 out 12 rhs (aref lhs 11))

	(vec4-in-mat4-mul-scal  12 out    rhs (aref lhs 12))
	(vec4-in-mat4-madd-scal 12 out  4 rhs (aref lhs 13))
	(vec4-in-mat4-madd-scal 12 out  8 rhs (aref lhs 14))
	(vec4-in-mat4-madd-scal 12 out 12 rhs (aref lhs 15))

	;; or alternatively

;;;	;; multiplicative part
;;;	(vec4-in-mat4-mul-scal   0 out    rhs (aref lhs  0))
;;;	(vec4-in-mat4-mul-scal   4 out    rhs (aref lhs  4))
;;;	(vec4-in-mat4-mul-scal   8 out    rhs (aref lhs  8))
;;;	(vec4-in-mat4-mul-scal  12 out    rhs (aref lhs 12))
;;;	
;;;	;; multiplicative-additive part
;;;	(vec4-in-mat4-madd-scal  0 out  4 rhs (aref lhs  1))
;;;	(vec4-in-mat4-madd-scal  4 out  4 rhs (aref lhs  5))
;;;	(vec4-in-mat4-madd-scal  8 out  4 rhs (aref lhs  9))
;;;	(vec4-in-mat4-madd-scal 12 out  4 rhs (aref lhs 13))
;;;	
;;;	(vec4-in-mat4-madd-scal  0 out  8 rhs (aref lhs  2))
;;;	(vec4-in-mat4-madd-scal  4 out  8 rhs (aref lhs  6))
;;;	(vec4-in-mat4-madd-scal  8 out  8 rhs (aref lhs 10))
;;;	(vec4-in-mat4-madd-scal 12 out  8 rhs (aref lhs 14))
;;;	
;;;	(vec4-in-mat4-madd-scal  0 out 12 rhs (aref lhs  3))
;;;	(vec4-in-mat4-madd-scal  4 out 12 rhs (aref lhs  7))
;;;	(vec4-in-mat4-madd-scal  8 out 12 rhs (aref lhs 11))
;;;	(vec4-in-mat4-madd-scal 12 out 12 rhs (aref lhs 15))
)

(defun mat4-empty()
	(make-array 16 :element-type 'single-float :adjustable nil :fill-pointer nil :displaced-to nil :initial-element 0.0)
)

(defun mat4-coerce(mat)
	;; (check-type mat (simple-vector 16))
	(declare (type (simple-vector 16) mat))
	(let* ((proper (mat4-empty)) (proper-dimension (array-dimension proper 0)))
		(declare (type mat4 proper))
		(dotimes (i proper-dimension proper)
			(setf (aref proper i) (coerce (aref mat i) 'single-float))
		)
	)
)

(defun mat4-dump(mat)
	;; (check-type mat mat4)
	(declare (type mat4 mat))
	(dotimes (i 4)
		(vec4-dump
			(vec4-make
				(aref mat (+ (* i 4) 0))
				(aref mat (+ (* i 4) 1))
				(aref mat (+ (* i 4) 2))
				(aref mat (+ (* i 4) 3))
			)
		)
	)
)

;;; global declarations

(defvar global-matrix
	(mat4-empty)
)

;;; timings
(defun timings (function)
	(let ((real-base (get-internal-real-time)) (run-base (get-internal-run-time)))
		(funcall function)
		(values (/ (- (get-internal-real-time) real-base) internal-time-units-per-second)
				(/ (- (get-internal-run-time) run-base) internal-time-units-per-second)
		)
	)
)

;;; main
(defun main ()
	(let ((lhs (mat4-coerce #(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16))) (rhs (mat4-coerce #(2 0 0 0 0 2 0 0 0 0 2 0 0 0 0 2))))
		(declare (type mat4 lhs))
		(declare (type mat4 rhs))
		(format t "~F~%" (timings (lambda () (dotimes (i 60000000) (mat4-mul global-matrix lhs rhs)))))
	)
	(mat4-dump global-matrix)
)
